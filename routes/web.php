<?php

use Illuminate\Support\Facades\Route;
Auth::routes();

Route::get('/lang/{lang}', [App\Http\Controllers\HomeController::class, 'lang'])->name('lang');
Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/search', [App\Http\Controllers\HomeController::class, 'search'])->name('search');
Route::get('/about', [App\Http\Controllers\HomeController::class, 'about'])->name('about');
Route::post('/subscribe', [App\Http\Controllers\SubscriberController::class, 'store'])->name('subscribe');
Route::post('/basket/{id}/{sign}', [App\Http\Controllers\BasketController::class, 'update'])->where('sign', 'p|m');
Route::get('/basket', [App\Http\Controllers\BasketController::class, 'index'])->name('basket.index');
Route::get('/getBasketData', [App\Http\Controllers\BasketController::class, 'getBasketData']);
Route::get('/books', [App\Http\Controllers\BookController::class, 'catalog'])->name('books.index');
Route::get('/books/{book}', [App\Http\Controllers\BookController::class, 'show'])->name('books.show');
Route::group([
    'prefix'        => '/cabinet',
    'middleware'    => ['auth'],
    'as'            => 'cabinet.'
], function(){
    Route::get('/', [App\Http\Controllers\CabinetController::class, 'index'])->name('index');
    Route::get('/account', [App\Http\Controllers\CabinetController::class, 'account'])->name('account');
    Route::post('/account', [App\Http\Controllers\CabinetController::class, 'accountUpdate'])->name('account.update');
    Route::view('calculator', 'cabinet.calculator')->name('calculator');
    Route::view('course', 'cabinet.cource')->name('course');
    Route::post('/connect', [App\Http\Controllers\CabinetController::class, 'TronAddressUpdate'])->name('tron.update');
});
Route::group([
    'prefix' => '/user',
    'middleware' => ['auth'],
    'as' => 'user.'
], function(){
    Route::get('/profile', [App\Http\Controllers\UserController::class, 'profile'])->name('profile.show');
    Route::post('/profile', [App\Http\Controllers\UserController::class, 'profileUpdate'])->name('profile.update');
});
Route::group([
    'prefix' => '/admin',
    'middleware' => ['auth', 'manager'],
    'as' => 'admin.'
], function(){
    Route::view('/', 'admin.index')->name('index');
    Route::resources([
        'orders' => App\Http\Controllers\OrderController::class,
        'partners' => App\Http\Controllers\PartnerController::class,
        'posts' => App\Http\Controllers\PostController::class,
        'books' => App\Http\Controllers\BookController::class,
        'categories' => App\Http\Controllers\CategoryController::class,
        'slides' => App\Http\Controllers\SlideController::class,
        'subscribers' => App\Http\Controllers\SubscriberController::class,
    ]);
});
Route::group([
    'prefix' => '/admin',
    'middleware' => ['auth', 'admin'],
    'as' => 'admin.'
], function(){
    Route::resources([
        'users' => App\Http\Controllers\UserController::class
    ]);
});
Route::post('/posts/{post}/comment', [App\Http\Controllers\PostController::class, 'comment'])->name('posts.comment');
Route::get('/{category}', [App\Http\Controllers\CategoryController::class, 'show'])->name('categories.show');
Route::get('/{category}/{post}', [App\Http\Controllers\PostController::class, 'show'])->name('posts.show');
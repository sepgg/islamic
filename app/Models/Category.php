<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
class Category extends Model
{

    use HasTranslations;
    public $timestamps = false;
    protected $translatable = ['name'];
    protected $fillable = ['menu', 'active', 'slug', 'pos'];
    public function posts(){
        return $this->hasMany(Post::class);
    }
}

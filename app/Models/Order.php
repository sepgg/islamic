<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function books(){
        return $this->belongsToMany(Book::class)->withPivot('q');
    }
}
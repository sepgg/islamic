<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
class Book extends Model
{
    use HasTranslations;
    public $translatable = ['name', 'text'];
    protected $fillable = ['image', 'price', 'active'];
    protected $casts = [
        'name'  => 'array'
    ];
}

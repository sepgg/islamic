<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
class Post extends Model
{
    use HasTranslations;
    public $translatable = ['name', 'text', 'preview'];
    protected $fillable = ['image', 'category_id', 'active', 'hot', 'slug'];
    protected $with = ['category'];
    public function comments(){
        return $this->morphMany(Comment::class, 'commentable')->latest();
    }
    public function category(){
        return $this->belongsTo(Category::class);
    }
}

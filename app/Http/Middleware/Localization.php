<?php

namespace App\Http\Middleware;
use App;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
class Localization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $lang = session()->get('lang');
        $langs = config('translatable.langs');
        if(!in_array($lang, array_keys($langs))) $lang = config('translatable.fallback_locale');
        
        App::setlocale($lang);
        View::share('lang', $lang);
        View::share('langs', $langs);
        return $next($request);
    }
}

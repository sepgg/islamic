<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;
use App\Models\Order;
class BasketController extends Controller
{
    public function index(){
        return view('basket');
    }
    public function getBasketData(Request $request){
        $basket = session()->get('basket', []);
        $items = Book::whereIn('id', array_keys($basket))->get();
        $resultItems = [];
        foreach($items as $item){
            $item->localedName = $item->name;
            $resultItems[$item->id] = $item;
        }
        return [
            'q'      => $basket,
            'items'  => $resultItems
        ];
    }
    public function update($id, $sign){
        $q = $sign == 'p' ? 1 : -1;
        $basket = session()->get('basket', []);
        if(!isset($basket[$id])) $basket[$id] = 1;
        else $basket[$id] += $q;
        if($basket[$id] < 1) unset($basket[$id]);
        session()->put('basket', $basket);
        $total = 0;
        foreach($basket as $q) $total += $q;
        return $total;
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Startup;
use App\Models\Slide;
use App\Models\Book;
use App\Models\Partner;
use App\Models\Category;
use App;
class HomeController extends Controller
{
    public function index(Request $request) {
        $posts = Post::whereActive(1)->latest()->limit(7)->get();
        $slides = Slide::all();
        $hot = Post::whereActive(1)->whereHot(1)->latest()->limit(4)->get();
        $books = Book::inRandomOrder()->limit(10)->get();
        $partners = Partner::all();
        $cat1 = Category::whereActive(1)->wherePos(1)->first();
        $cat2 = Category::whereActive(1)->wherePos(2)->first();
        $pos1 = false;
        $pos2 = false;
        if($cat1) $pos1 = $cat1->posts()->whereActive(1)->latest()->limit(4)->get();
        if($cat2) $pos2 = $cat2->posts()->whereActive(1)->latest()->limit(4)->get();
        return view('home', compact('posts', 'slides', 'books', 'pos1', 'pos2', 'cat1', 'cat2', 'partners', 'hot'));
    }
    public function search(Request $request){
        $q = $request->q;
        $posts = false;
        $count = 0;
        if($q){
            $locale = App::getLocale();
            $posts = Post::with(['category' => function($query){
                $query->whereActive(1);
            }])->whereActive(1)->where(function($query) use ($q, $locale){
                $query->where("name->$locale", 'like', "%$q%")->orWhere("preview->$locale", 'like', "%$q%")->orWhere("text->$locale", 'like', "%$q%");
            })->latest()->paginate();
            $count = $posts->count();
        }
        return view('search', compact('posts', 'q', 'count'));
    }
    public function lang($locale){
        App::setlocale($locale);
        session()->put('lang', $locale);
        return redirect()->back();
    }
    public function about(){
        $posts = Post::latest()->limit(2)->get();
        return view('about', compact('posts'));
    }
}

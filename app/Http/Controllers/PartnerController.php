<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Partner;
use Storage;
class PartnerController extends Controller
{
    public function index(){
        $partners = Partner::paginate(50);
        return view('admin.partners.index', compact('partners'));
    }
    public function create(){
        return view('admin.partners.create');
    }
    public function edit(Partner $partner){
        return view('admin.partners.edit', compact('partner'));
    }
    public function store(Request $request){
        $request->validate([
            'image' => 'required|image|max:4096'
        ]);
        if($request->hasFile('image')) $data['image'] = $request->file('image')->store('partnes');
        Partner::create($data);
        session()->flash('message', 'Партнер успешно добавлен');
        return redirect()->route('admin.partners.index');
    }
    public function update(Request $request, Partner $partner){
        $request->validate([
            'image' => 'required|image|max:4096'
        ]);
        $data = $request->except('image');
        if($request->hasFile('image')){
            if($partner->image) Storage::delete($partner->image);
            $data['image'] = $request->file('image')->store('partnes');
        }
        $partner->update($data);
        session()->flash('message', 'Партнер успешно изменен');
        return redirect()->route('admin.partners.index');
    }
    public function destroy(Partner $partner){
        if($partner->image) Storage::delete($partner->image);
        $partner->delete();
        session()->flash('message', 'Партнер успешно удален');
        return redirect()->route('admin.partners.index');
    }
}

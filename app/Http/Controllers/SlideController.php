<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Slide;
use Storage;
class SlideController extends Controller
{
    public function index(){
        $slides = Slide::paginate(50);
        return view('admin.slides.index', compact('slides'));
    }
    public function create(){
        return view('admin.slides.create');
    }
    public function edit(Slide $slide){
        return view('admin.slides.edit', compact('slide'));
    }
    public function store(Request $request){
        $request->validate([
            'name_en' => 'nullable|string|max:255',
            'image' => 'required|image'
        ]);
        $data = [];
        if($request->hasFile('image')) $data['image'] = $request->file('image')->store('slides');
        $slide = Slide::create($data);
        foreach(config('translatable.available_locale') as $locale){
            $slide->setTranslation('name', $locale, $request->get("name_$locale"));
        }
        $slide->save();
        session()->flash('message', 'Новый слайд успешно добавлен');
        return redirect()->route('admin.slides.index');
    }
    public function update(Request $request, Slide $slide){
        $request->validate([
            'name_en' => 'nullable|string|max:255',
            'image' => 'nullable|image'
        ]);
        $data = [];
        if($request->hasFile('image')){
            if($slide->image) Storage::delete($slide->image);
            $data['image'] = $request->file('image')->store('slides');
        }
        $slide->update($data);
        foreach(config('translatable.available_locale') as $locale){
            $slide->setTranslation('name', $locale, $request->get("name_$locale"));
        }
        $slide->save();
        session()->flash('message', 'Слайд успешно изменен');
        return redirect()->route('admin.slides.index');
    }
    public function destroy(Slide $slide){
        if($slide->image) Storage::delete($slide->image);
        $slide->delete();
        session()->flash('message', 'Слайд успешно удален');
        return redirect()->route('admin.slides.index');
    }
}

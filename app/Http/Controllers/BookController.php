<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;
use Storage;
class BookController extends Controller
{
    public function index(){
        $books = Book::paginate(50);
        return view('admin.books.index', compact('books'));
    }
    public function catalog(){
        $books = Book::paginate();
        return view('books.index', compact('books'));
    }
    public function create(){
        return view('admin.books.create');
    }
    public function show(Book $book){
        $books = Book::where('id', '!=', $book->id)->inRandomOrder()->limit(4)->get();
        return view('books.show', compact('book', 'books'));
    }
    public function edit(Book $book){
        return view('admin.books.edit', compact('book'));
    }
    public function frontIndex(){
        $books = Book::paginate(20);
        return view('books.index', compact('books'));
    }
    public function store(Request $request){
        $request->validate([
            'name_en' => 'required|string|max:255',
            'text_en' => 'required',
            'image' => 'nullable|image|max:9000',
            'price' => 'required|numeric'
        ]);
        $data = $request->except('image');
        if($request->hasFile('image')){
            $data['image'] = $request->file('image')->store('books');
        }
        $book = Book::create($data);
        foreach(config('translatable.available_locale') as $locale){
            $book->setTranslation('name', $locale, $request->get("name_$locale"));
            $book->setTranslation('text', $locale, $request->get("text_$locale"));
        }
        $book->save();
        session()->flash('success', 'Новая книга успешно добавлена');
        return redirect()->route('admin.books.index');
    }
    public function update(Request $request, Book $book){
        $request->validate([
            'name_en' => 'required|string|max:255',
            'text_en' => 'required',
            'image' => 'nullable|image',
            'price' => 'required|numeric'
        ]);
        $data = $request->except('image');
        if($request->hasFile('image')){
            if($book->image) Storage::delete($book->image);
            $data['image'] = $request->file('image')->store('books');
        }
        $book->update($data);
        foreach(config('translatable.available_locale') as $locale){
            $book->setTranslation('name', $locale, $request->get("name_$locale"));
            $book->setTranslation('text', $locale, $request->get("text_$locale"));
        }
        $book->save();
        session()->flash('success', 'Книга успешно изменена');
        return redirect()->route('admin.books.index');
    }
    public function destroy(Book $book){
        if($book->image) Storage::delete($book->image);
        $book->delete();
        session()->flash('success', 'Запись успешно удалена');
        return redirect()->route('admin.books.index');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Category;
use Storage;
use Str;
class PostController extends Controller
{
    public function index(){
        $posts = Post::withCount('comments')->latest()->paginate(50);
        return view('admin.posts.index', compact('posts'));
    }
    public function create(){
        $categories = Category::pluck('name', 'id');
        return view('admin.posts.create', compact('categories'));
    }
    public function show($category, $slug){
        $category = Category::whereSlug($category)->whereActive(1)->firstOrFail();
        $post = Post::with('category', 'comments')
            ->whereActive(1)
            ->where('category_id', $category->id)
            ->where('slug', $slug)
            ->firstOrFail();
        $latest = Post::whereActive(1)->where('id', '!=', $post->id)->latest()->limit(5)->get();
        return view('posts.show', compact('post', 'latest'));
    }
    public function edit(Post $post){
        $post->load('category');
        $categories = Category::pluck('name', 'id');
        return view('admin.posts.edit', compact('post', 'categories'));
    }
    public function store(Request $request){
        $request->validate([
            'name_en' => 'required|string|max:255',
            'image' => 'nullable|image|max:9000'
        ]);
        $data = $request->except('image');
        $data['hot'] = $request->has('hot');
        $slug = Str::slug($request->name_en);
        if(Post::whereSlug($slug)->count()) return back()->withInput()->withErrors(['name_en' => 'Новость с таким названием уже существует']);
        $data['slug'] = $slug;
        $data['active'] = $request->has('active');
        if($request->hasFile('image')){
            $data['image'] = $request->file('image')->store('posts');
        }
        $post = Post::create($data);
        foreach(config('translatable.available_locale') as $locale){
            $post->setTranslation('name', $locale, $request->get("name_$locale"));
            $post->setTranslation('preview', $locale, $request->get("preview_$locale"));
            $post->setTranslation('text', $locale, $request->get("text_$locale"));
        }
        $post->save();
        session()->flash('success', 'Новая запись успешно добавлена');
        return redirect()->route('admin.posts.index');
    }
    public function update(Request $request, Post $post){
        $request->validate([
            'name_en' => 'required|string|max:255',
            'image' => 'nullable|image'
        ]);
        $data = $request->except('image');
        $data['hot'] = $request->has('hot');
        $slug = Str::slug($request->name_en);
        $dublicate = Post::whereSlug($slug)->first();
        if($dublicate && $dublicate->id != $post->id) return back()->withInput()->withErrors(['name_en' => 'Новость с таким названием уже существует']);
        $data['slug'] = $slug;
        $data['active'] = $request->has('active');
        if($request->hasFile('image')){
            if($post->image) Storage::delete($post->image);
            $data['image'] = $request->file('image')->store('posts');
        }
        $post->update($data);
        foreach(config('translatable.available_locale') as $locale){
            $post->setTranslation('name', $locale, $request->get("name_$locale"));
            $post->setTranslation('preview', $locale, $request->get("preview_$locale"));
            $post->setTranslation('text', $locale, $request->get("text_$locale"));
        }
        $post->save();
        session()->flash('success', 'Запись успешно изменена');
        return redirect()->route('admin.posts.index');
    }
    public function destroy(Post $post){
        if($post->image) Storage::delete($post->image);
        $post->delete();
        session()->flash('success', 'Запись успешно удалена');
        return redirect()->route('admin.posts.index');
    }
    public function comment(Request $request, Post $post){
        $request->validate([
            'text' => 'required'
        ]);
        $data = $request->only(['text']);
        if(auth()->check()) $data['user_id']  = auth()->id();
        $post->comments()->create($data);
        session()->flash('success', 'Комментарий успешно оставлен');
        return back();
    }
}

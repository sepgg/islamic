<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Post;
use Str;
class CategoryController extends Controller
{
    public function index(){
        $categories = Category::paginate(50);
        return view('admin.categories.index', compact('categories'));
    }
    public function create(){
        return view('admin.categories.create');
    }
    public function show($slug){
        $category = Category::whereActive(1)->where('slug', $slug)->firstOrFail();
        $top = $category->posts()->whereActive(1)->latest()->limit(3)->get();
        $posts = $category->posts()->whereActive(1)->latest()->offset(3)->paginate();
        $latest = $category->posts()->whereActive(1)->latest()->limit(5)->get();
        return view('categories.show', compact('category', 'posts', 'latest', 'top'));
    }
    public function store(Request $request){
        $request->validate([
            'name_en' => 'required|string|max:255'
        ]);
        $data = $request->only('pos');
        if($data['pos']) Category::where('pos', $request->pos)->update(['pos' => NULL]);
        $data['slug'] = Str::slug($request->name_en, '-');
        $data['active'] = $request->has('active');
        $data['menu'] = $request->has('menu');
        $category = Category::create($data);
        foreach(config('translatable.available_locale') as $locale){
            $category->setTranslation('name', $locale, $request->get("name_$locale"));
        }
        $category->save();
        session()->flash('success', 'Новая ребрика успешно добавлена');
        return redirect()->route('admin.categories.index');
    }
    public function edit(Category $category){
        return view('admin.categories.edit', compact('category'));
    }
    public function update(Request $request, Category $category){
        $request->validate([
            'name_en' => 'required|string|max:255'
        ]);
        $data = $request->only('pos');
        if($data['pos']) Category::where('id', '!=', $category->id)->where('pos', $request->pos)->update(['pos' => NULL]);
        $data['slug'] = Str::slug($request->name_en, '-');
        $data['menu'] = $request->has('menu');
        $data['active'] = $request->has('active');
        $category->update($data);
        foreach(config('translatable.available_locale') as $locale){
            $category->setTranslation('name', $locale, $request->get("name_$locale"));
        }
        $category->save();
        session()->flash('success', 'Рубрика успешно изменена');
        return redirect()->route('admin.categories.index');
    }
    public function destroy(Category $category){
        $category->delete();
        cache()->forever('categories', Category::whereActive(1)->whereMenu(1)->get());
        session()->flash('success', 'Рубрика успешно удалена');
        return redirect()->route('admin.categories.index');
    }
}

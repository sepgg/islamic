<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
class OrderController extends Controller
{
    public function index(){
        $orders = Order::latest()->paginate();
        return view('admin.orders.index', compact('orders'));
    }
    public function store(Request $request){
        $basket = Order::basket();
        $order = Order::create();
        $data = [];
        foreach($basket as $id => $q)
            $data[$id] = [
                'q' => $q
            ];
        $order->books()->sync($data);
        session()->put('basket', []);
    }
}

<?php

return [
    'fallback_locale' => 'en',
    'available_locale'  => ['en', 'ru', 'ar', 'tr', 'fr'],
    'langs' => [
        'en'    => 'English',
        'ru'    => 'Russian',
        'ar'    => 'Arabic',
        'tr'    => 'Turkish',
        'fr'    => 'France'
    ]
];

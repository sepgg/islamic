require('./bootstrap')
window.Vue = require('vue').default;
import axios from "axios";
import store from "./store";
const files = require.context('./', true, /\.vue$/i)
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

new Vue({
    async created(){
        this.$store.dispatch('loadBasket')
    },
    store,
    el: '#app',
});

$(document).ready(function(){
    $(document).click(e => {
        if(!$(e.target).closest('.basket').length) $('.basket').removeClass('active')
    })
    $('.basket__title-close').click(function(){
        $('.basket').removeClass('active')
    })
    $('[data-toggle="tolltip"]').tooltip();
    if(window.tinymce){
        tinymce.init({
            selector: '.editor',
            menubar:false,
            statusbar: false,
            height: 500,
            media_live_embeds: true,
            media_poster: false,
            extended_valid_elements : "script[language|type|async|src|charset]",
            plugins: [
                'advlist autolink lists link charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table paste wordcount'
            ],
            toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | code fullscreen |',
        });
    }
    $('.confirmed').submit(function(e){
        if(!confirm('Подтвердите действие!')){
            e.preventDefault()
            return false
        }
    })
    $('.addtocart').click(function(e){
        e.preventDefault();
        e.stopPropagation();
        const id = $(this).data('id')
        axios.post('/basket/' + id + '/p')
        .then(function(response){
            $('.ind-header__nav-item-basket-count').text(response.data)
        })
    })
})
import Vue from 'vue'
import Vuex from 'vuex'
Vue.use( Vuex )
export default new Vuex.Store( {
    state: {
        totalPrice: 0,
        totalItems: 0,
        items: [],
        q: {}
    },
    actions: {
        async loadBasket({commit}){
            const response = await fetch('/getBasketData')
            const data = await response.json()
            commit('setBasket', data)
        }
    },
    mutations: {
        setBasket(state, data){
            let totalPrice = 0
            for(let p in data.items) totalPrice += data.items[p].price * data.q[p]
            state.totalPrice = totalPrice;
            state.items = data.items
            state.q = data.q
            let totalItems = 0
            for(let p in data.q) totalItems += data.q[p]
            state.totalItems = totalItems
        }
    }
} )

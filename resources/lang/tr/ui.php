<?php

return [
    'title' => 'Islamic news',
    'home'  => 'Ev',
    'books' => 'Kitaplar',
    'payment'   => 'Ödeme',
    'subscribe' => [
        'success' => 'Abone başarılı',
        'active'     => 'E-posta zaten kullanılıyor',
        'validate'  => 'Yazın doğru e-posta adresi',
        'title'     => 'Haber bültenine abone olun',
        'placeholder'   => 'E-posta adresinizi '
    ],
    'news'  => 'Haberler',
    'buy'   => 'Almak',
    'readdemo'  => 'Demoyu okuyun',
    'latest'    => 'En Son Haberler',
    'follow'    => 'Bizi takip edin',
    'readmore'  => 'daha fazla oku',
    'categories'    => 'Kategoriler',
    'rights'    => 'Islamic news.Tüm hakkı saklıdır',
    'cart'  => [
        'title' => 'Sepetiniz',
        'total' => 'toplam',
        'checkout'  => 'Ödeme'
    ],
    'comments'  => [
        'title' => 'Yorum',
        'placeholder'   => 'Yorumunuzu buraya yazın',
        'button'    => 'Şimdi Gönder',
        'guest'     => 'misafir'
    ],
    'email_not_found'   => 'E-posta bulunamadı',
    'reset_password'    => 'Şifreyi sıfırla',
    'recovery_password' => 'Şifre kurtarma',
    'already_user'      => 'Zaten kayıtlı?',
    'forgot_password'   => 'Şifremi unuttum?',
    'login'              => 'Oturum aç',
    'register'          => 'Kaydolmak',
    'no_account'        => 'Hesabınız yok?',
    'your_email'         => 'E-posta',
    'your_pass'         => 'Şifreniz',
    'repeat_pass'       => 'Parolanızı tekrarlayın',
    'shop'              => 'dükkan'
]; 

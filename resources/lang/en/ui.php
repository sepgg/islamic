<?php

return [
    'title' => 'Islamic news',
    'home'  => 'Home',
    'books' => 'Books',
    'payment'   => 'Payment',
    'subscribe' => [
        'success' => 'Subscribe successful',
        'active'     => 'The email already be used',
        'validate'  => 'Type correct email address',
        'title'     => 'Subscribe to the newsletter',
        'placeholder'   => 'Your email address'
    ],
    'news'  => 'News',
    'buy'   => 'Buy',
    'readdemo'  => 'Read the demo',
    'latest'    => 'The Latest News',
    'follow'    => 'Follow Us',
    'readmore'  => 'Read More',
    'categories'    => 'Categories',
    'rights'    => 'Islamic news.All right reserved',
    'cart'  => [
        'title' => 'Your cart',
        'total' => 'total',
        'checkout'  => 'Checkout'
    ],
    'comments'  => [
        'title' => 'Comment',
        'placeholder'   => 'Write your comment in here',
        'button'    => 'Post Now',
        'guest'     => 'Гость'
    ],
    'email_not_found'   => 'E-mail not found',
    'reset_password'    => 'Reset password',
    'recovery_password' => 'Password recovery',
    'already_user'      => 'Already registered?',
    'forgot_password'   => 'Forgot password?',
    'login'              => 'Login',
    'register'          => 'Register',
    'no_account'        => 'Not have account?',
    'your_email'         => 'Your E-mail',
    'your_pass'         => 'Your password',
    'repeat_pass'       => 'Repeat your password',
    'shop'              => 'Shop',
    "basket"        => [
        'total' => "Total",
        'delivery_method'  => "Delivery method",
        'delivery_address'  => 'Delivery address',
        'date_time' => 'Date & time',
        'payment'   => 'Payment',
    ]
]; 

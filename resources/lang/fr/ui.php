<?php

return [
    'title' => 'Islamic news',
    'home'  => 'Maison',
    'books' => 'Livre',
    'payment'   => 'Paiement',
    'subscribe' => [
        'success' => "S'abonner avec succès",
        'active'     => "L'e-mail déjà utilisé",
        'validate'  => "Tapez l'adresse e-mail correcte",
        'title'     => 'Abonnez-vous à la newsletter',
        'placeholder'   => 'Votre adresse e-mail'
    ],
    'news'  => 'Nouvelles',
    'buy'   => 'Acheter',
    'readdemo'  => 'Lire la démo',
    'latest'    => 'Les Dernières Nouvelles',
    'follow'    => 'Suivez-Nous',
    'readmore'  => 'Lire La Suite',
    'categories'    => 'Catégorie',
    'rights'    => 'Islamic news.Tous droits réservés',
    'cart'  => [
        'title' => 'Votre panier',
        'total' => 'total',
        'checkout'  => 'Caisse'
    ],
    'comments'  => [
        'title' => 'Commentaire',
        'placeholder'   => 'Écrivez votre commentaire ici',
        'button'    => 'Publier Maintenant',
        'guest'     => 'invité'
    ],
    'email_not_found'   => 'Courriel introuvable',
    'reset_password'    => 'Réinitialiser le mot de passe',
    'recovery_password' => 'Récupération de mot de passe',
    'already_user'      => 'Déjà inscrit?',
    'forgot_password'   => 'Mot de passe oublié?',
    'login'              => 'Connexion',
    'register'          => 'Inscrire',
    'no_account'        => 'Ne pas avoir de compte?',
    'your_email'         => 'Votre e-mail',
    'your_pass'         => 'Votre mot de passe',
    'repeat_pass'       => 'Répétez votre mot de passe',
    'shop'              => 'boutique'
]; 

@extends('layouts.site')

@section('content')
<section class="ind-last">
    <div class="containers">
        <form action="{{route('register')}}" method="post" class="modelLogin__reg" style="margin: 0 auto;">
            @csrf
            <div class="modelLogin__reg-title">{{__('ui.register')}}</div>
            <div class="modelLogin__reg-email">
                <input type="text" placeholder="E-mail" name="email">
                <div class="modelLogin__reg-email-add">{{__('ui.your_email')}}</div>
            </div>
            <div class="modelLogin__reg-password">
                <input type="password" placeholder="{{__('ui.your_pass')}}" name="password">
                <div class="modelLogin__reg-password-add">{{__('ui.your_pass')}}</div>
            </div>
            <div class="modelLogin__reg-passwords">
                <input type="password" placeholder="{{__('ui.your_pass')}}" name="password_confirmation">
                <div class="modelLogin__reg-password-add">{{__('ui.repeat_pass')}}</div>
            </div>
            <button class="modelLogin__reg-btn">{{__('ui.your_pass')}}</button>
            <a href="{{route('password.request')}}" class="modelLogin__entry-link">{{__('ui.forgot_password')}}</a>
            <div class="modelLogin__reg-reg">
                <p>{{__('ui.already_user')}}</p>
                <a href="{{route('login')}}">{{__('ui.login')}}</a>
            </div>
        </form>
    </div>
</section>
@endsection

@extends('layouts.site')

@section('content')
<section class="ind-last">
    <div class="containers">
        <form method="POST" action="{{ route('password.email') }}" class="modelLogin__entry" style="margin: 0 auto;">
            @csrf
            <div class="modelLogin__recovery-title">{{__('ui.recovery_password')}}</div>
            <div class="modelLogin__recovery-email">
                <input type="text" placeholder="E-mail" name="email" alue="{{ old('email') }}" required autocomplete="email" autofocus>
                <div class="modelLogin__reg-email-add">{{__('ui.your_email')}}</div>
            </div>
            @error('email')
                <div class="modelLogin__recovery-error display-n">__('ui.email_not_found')</div>
            @enderror
            
            <button class="modelLogin__recovery-btn">{{__('ui.reset_password')}}</button>
            <div class="modelLogin__recovery-reg">
                <p>{{__('ui.already_user')}}</p>
                <a href="{{route('login')}}">{{__('ui.login')}}</a>
            </div>
        </form>
    </div>
</section>
@endsection

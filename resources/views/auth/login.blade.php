@extends('layouts.site')

@section('content')
<section class="ind-last">
    <div class="containers">
        <form action="{{route('login')}}" method="post" class="modelLogin__entry" style="margin: 0 auto;">
            @csrf
            <div class="modelLogin__entry-title">{{__('ui.login')}}</div>
            <div class="modelLogin__entry-email">
                <input type="text" placeholder="E-mail" name="email" value="{{old('email')}}">
                <div class="modelLogin__reg-email-add">{{__('ui.your_email')}}</div>
            </div>
            <div class="modelLogin__entry-password">
                <input type="password" placeholder="{{__('ui.your_pass')}}" name="password">
                <div class="modelLogin__reg-password-add">{{__('ui.your_pass')}}</div>
            </div>
            <button class="modelLogin__entry-btn">{{__('ui.login')}}</button>
            <a href="{{route('password.request')}}" class="modelLogin__entry-link">{{__('ui.forgot_password')}}</a>
            <div class="modelLogin__entry-reg">
                <p>{{__('ui.no_account')}}</p>
                <a href="{{route('register')}}">{{__('ui.register')}}</a>
            </div>
        </form>
    </div>
</section>
@endsection

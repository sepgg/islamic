@extends('layouts.site')

@section('content')
<section class="pay">
        <div class="containers">
            <div class="pay__block">
                <div class="pay__block-info">
                    <div class="pay__block-info-method">
                        <div class="pay__block-info-method-title">{{__('ui.basket.delivery_method')}}</div>
                        <div class="pay__block-info-method-type">
                            <div class="pay__block-info-method-items" id="payMethodItems-1">
                                <div class="pay__block-info-method-items-circle"></div>
                                <div class="pay__block-info-method-items-text">
                                    <h3>Title</h3>
                                    <p>Description</p>
                                </div>
                                <div class="pay__block-info-method-items-price">Price</div>
                            </div>
                            <div class="pay__block-info-method-items pay__block-info-method-items-active" id="payMethodItems-2">
                                <div class="pay__block-info-method-items-circle pay__block-info-method-items-circle-active"></div>
                                <div class="pay__block-info-method-items-text">
                                    <h3>Title</h3>
                                    <p>Description</p>
                                </div>
                                <div class="pay__block-info-method-items-price">Price</div>
                            </div>
                        </div>
                    </div>
                    <div class="pay__block-info-address">
                        <div class="pay__block-info-address-title">{{__('ui.basket.delivery_address')}}</div>
                        <div class="pay__block-info-address-type">
                            <div class="pay__block-info-address-items">text</div>
                            <div class="pay__block-info-address-items">text</div>
                        </div>
                    </div>
                    <div class="pay__block-info-date">
                        <div class="pay__block-info-date-title">{{__('ui.basket.date_time')}}</div>
                        <div class="pay__block-info-date-type">
                            <div class="pay__block-info-date-items">text</div>
                            <div class="pay__block-info-date-items">text</div>
                        </div>
                    </div>
                    <div class="pay__block-info-line"></div>
                    <div class="pay__block-info-btns">
                        <button class="pay__block-info-btn">
                            <p>{{__('ui.basket.payment')}}</p>
                            <svg width="11" height="18" viewBox="0 0 11 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.292969 1.70697L7.58597 8.99997L0.292969 16.293L1.70697 17.707L10.414 8.99997L1.70697 0.292969L0.292969 1.70697Z" fill="black"/></svg>

                        </button>
                    </div>
                </div>
                <div class="pay__block-card">
                    <basket lang="{{$lang}}" total="{{__('ui.basket.total')}}"></basket>
                </div>
            </div>
        </div>
    </section>
@stop
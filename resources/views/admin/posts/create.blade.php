@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col">Новая запись</div>
    <div class="col text-right"><a href="{{route('admin.posts.index')}}" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left"></i></a></div>
</div>
<hr>
<form action="{{route('admin.posts.store')}}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="form-group form-check">
        <input name="active" type="checkbox" class="form-check-input" id="active">
        <label class="form-check-label" for="active">Активна</label>
    </div>
    <div class="form-group form-check">
        <input name="hot" type="checkbox" class="form-check-input" id="hot">
        <label class="form-check-label" for="hot">Горячая</label>
    </div>
    <div class="form-group">
        <label>Категория</label>
        <select name="category_id" class="form-control">
            <option value="-" disabled selected></option>
            @foreach($categories as $id => $name)
            <option value="{{$id}}" @if(old('category_id') == $id) selected @endif>{{$name}}</option>
            @endforeach
        </select>
    </div>
    <nav>
        <div class="nav nav-tabs" role="tablist">
            @foreach(config('translatable.available_locale') as $key => $locale)
            <a class="nav-item nav-link @if(!$key) active @endif" data-toggle="tab" href="#{{$locale}}" role="tab">{{$locale}}</a>
            @endforeach
        </div>
    </nav>
    <div class="tab-content pt-3">
        @foreach(config('translatable.available_locale') as $key => $locale)
            <div class="tab-pane fade @if(!$key) show active @endif" id="{{$locale}}" role="tabpanel">
                <div class="form-group">
                    <label>Заголовок</label>
                    <input type="text" class="@error("name_$locale") is-invalid @enderror form-control" name="name_{{$locale}}" value="{{old("name_$locale")}}">
                    @error("name_$locale")<span class="invalid-feedback" role="alert">{{ $message }}</span>@enderror
                </div>
                <div class="form-group">
                    <label>Вводный текст</label>
                    <textarea name="preview_{{$locale}}" rows="2" class="@error("preview_$locale") is-invalid @enderror form-control">{{old("preview_$locale")}}</textarea>
                    @error("preview_$locale")<span class="invalid-feedback" role="alert">{{ $message }}</span>@enderror
                </div>
                <div class="form-group">
                    <label>Детальный текст</label>
                    <textarea name="text_{{$locale}}" rows="10" class="@error("text_$locale") is-invalid @enderror form-control editor">{{old("text_$locale")}}</textarea>
                    @error("text_$locale")<span class="invalid-feedback" role="alert">{{ $message }}</span>@enderror
                </div>
            </div>
        @endforeach
    </div>
    <div class="form-group">
        <label>Изображение <input type="file" name="image"></label>
    </div>
    <div class="form-group">
        <button class="btn btn-success">Сохранить</button>
    </div>
</form>
@stop
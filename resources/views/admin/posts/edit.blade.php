@extends('layouts.admin')

@section('content')
<div class="d-flex align-items-center justify-content-between">
    <span>Редактирование записи</span>
    <a href="{{route('admin.posts.index')}}" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left"></i></a>
</div>
<form action="{{route('admin.posts.update', $post->id)}}" method="post" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="form-group form-check">
        <input name="active" type="checkbox" class="form-check-input" id="active" @if($post->active) checked @endif>
        <label class="form-check-label" for="active">Активна</label>
    </div>
    <div class="form-group form-check">
        <input name="hot" type="checkbox" class="form-check-input" id="hot" @if($post->hot) checked @endif>
        <label class="form-check-label" for="hot">Горячая</label>
    </div>
    <div class="form-group">
        <label>Категория</label>
        <select name="category_id" class="form-control">
            <option value="-" disabled selected></option>
            @foreach($categories as $id => $name)
            <option value="{{$id}}" @if(old('category_id', $post->category_id) == $id) selected @endif>{{$name}}</option>
            @endforeach
        </select>
    </div>
    <nav>
        <div class="nav nav-tabs" role="tablist">
            @foreach(config('translatable.available_locale') as $key => $locale)
            <a class="nav-item nav-link @if(!$key) active @endif" data-toggle="tab" href="#{{$locale}}" role="tab">{{$locale}}</a>
            @endforeach
        </div>
    </nav>
    <div class="tab-content pt-3">
        @foreach(config('translatable.available_locale') as $key => $locale)
            <?php $post->setLocale($locale); ?>
            <div class="tab-pane fade @if(!$key) show active @endif" id="{{$locale}}" role="tabpanel">
                <div class="form-group">
                    <label>Заголовок</label>
                    <input type="text" class="form-control @error("name_$locale") is-invalid @enderror" name="name_{{$locale}}" value="{{old("name_$locale", $post->name)}}">
                    @error("name_$locale")<span class="invalid-feedback" role="alert">{{ $message }}</span>@enderror
                </div>
                <div class="form-group">
                    <label>Вводный текст</label>
                    <textarea name="preview_{{$locale}}" rows="2" class="form-control @error("preview_$locale") is-invalid @enderror">{{old("preview_$locale", $post->preview)}}</textarea>
                    @error("preview_$locale")<span class="invalid-feedback" role="alert">{{ $message }}</span>@enderror
                </div>
                <div class="form-group">
                    <label>Детальный текст</label>
                    <textarea name="text_{{$locale}}" rows="7" class="form-control editor @error("text_$locale") is-invalid @enderror">{{old("text_$locale", $post->text)}}</textarea>
                    @error("text_$locale")<span class="invalid-feedback" role="alert">{{ $message }}</span>@enderror
                </div>
            </div>
        @endforeach
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group @error('image') is-invalid @enderror">
                <label>Изображение <input type="file" name="image"></label>
                @error('image')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        @if($post->image)
        <div class="col-md-6">
            <img src="/storage/{{$post->image}}" alt="{{$post->name}}" class="img-fluid">
        </div>
        @endif
    </div>
    <div class="form-group">
        <button class="btn btn-success">Сохранить</button>
    </div>
</form>
@stop
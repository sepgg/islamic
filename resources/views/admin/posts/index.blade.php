@extends('layouts.admin')

@section('content')
<div class="d-flex align-items-center justify-content-between">
    <span>Список новостей</span>
    <span><a href="{{route('admin.posts.create')}}" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Добавить запись</a></span>
</div>
{{$posts->links()}}
<table class="table">
    <tr>
        <th>Заголовок</th>
        <th>Категория</th>
        <th>Активная</th>
        <th>Горячая</th>
        <th width="100px">Действия</th>
    </tr>
    @foreach($posts as $item)
    <tr>
        <td>{{$item->name}}</td>
        <td>{{$item->category->name ?? 'Не выбрана'}}</td>
        <td>@if($item->active) <i class="fa fa-check text-success"></i> @endif</td>
        <td>@if($item->hot) <i class="fa fa-check text-success"></i> @endif</td>
        <td>
            <a href="{{route('admin.posts.edit', $item->id)}}" class="btn btn-info btn-sm"><i class="fa fa-pencil"></i></a>
            <form action="{{route('admin.posts.destroy', $item->id)}}" class="d-inline-block confirmed" method="post">
                @csrf
                <input type="hidden" name="_method" value="DELETE">
                <button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
            </form>
        </td>
    </tr>
    @endforeach
</table>
{{$posts->links()}}
@stop
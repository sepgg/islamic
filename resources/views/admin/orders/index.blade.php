@extends('layouts.admin')

@section('content')
@if($orders->count())
    {{$orders->links()}}
    <table class="table">
        <tr>
            <th>Статус</th>
            <th>Сумма</th>
            <th>Дата</th>
            <th>Действия</th>
        </tr>
        @foreach($orders as $item)
        <tr>
            <td>{{$orders->email}}</td>
            <td>{{$orders->created_at->format('d.m.Y H:i')}}</td>
            <td>
                <form action="{{route('admin.orders.destroy', $item->id)}}" class="d-inline-block confirmed" method="post">
                    @csrf
                    <input type="hidden" name="_method" value="DELETE">
                    <button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
    {{$orders->links()}}
@else
В системе пока нет ни одного заказа
@endif
@stop
@extends('layouts.admin')

@section('content')
    <form action="{{route('admin.partners.store')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label>Картинка <input type="file" name="image"></label>
        </div>
        <div class="form-group">
            <button class="btn btn-primary">Сохранить</button>
        </div>
    </form>
@stop
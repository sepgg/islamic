@extends('layouts.admin')

@section('content')
<div class="d-flex align-items-center justify-content-between">
    <span>Список Партнеров</span>
    <span><a href="{{route('admin.partners.create')}}" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Добавить запись</a></span>
</div>
{{$partners->links()}}
<table class="table">
    <tr>
        <th width="200px">Лого</th>
        <th width="100px">Действия</th>
    </tr>
    @foreach($partners as $item)
    <tr>
        <td><img src="/storage/{{$item->image}}" class="img-fluid" alt=""></td>
        <td>
            <a href="{{route('admin.partners.edit', $item->id)}}" class="btn btn-sm btn-info"><i class="fa fa-pencil"></i></a>
            <form action="{{route('admin.partners.destroy', $item->id)}}" class="d-inline-block confirmed" method="post">
                @csrf
                @method('delete')
                <button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
            </form>
        </td>
    </tr>
    @endforeach
</table>
{{$partners->links()}}
@stop
@extends('layouts.admin')

@section('content')
    <form action="{{route('admin.partners.update', $partner->id)}}" method="post" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="row">
            <div class="col-md-6">
                <div class="form-group @error('image') has-invalid @enderror">
                    <label>Картинка <input type="file" name="image"></label>
                    @error('image')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            @if($partner->image)
            <div class="col-md-6">
                <img src="/storage/{{$partner->image}}" alt="{{$partner->name}}" class="img-fluid">
            </div>
            @endif
        </div>
        <div class="form-group">
            <button class="btn btn-primary">Сохранить</button>
        </div>
    </form>
@stop
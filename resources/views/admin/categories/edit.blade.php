@extends('layouts.admin', ['right' => false])

@section('content')
<div class="d-flex align-items-center justify-content-between">
    <span>Редактирование рубрики</span>
    <span>
        <a href="{{route('admin.categories.index')}}" class="btn btn-sm btn-primary" data-toggle="tooltip" title="К списку"><i class="fa fa-arrow-left"></i></a>
    </span>
</div>
<hr>
<form action="{{route('admin.categories.update', $category->id)}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group form-check">
        <input name="active" type="checkbox" class="form-check-input" id="active" @if($category->active) checked @endif>
        <label class="form-check-label" for="active">Активна</label>
    </div>
    <div class="form-group form-check">
        <input name="menu" type="checkbox" class="form-check-input" id="menu" @if(old('menu', $category->menu)) checked @endif>
        <label class="form-check-label" for="menu">В меню</label>
    </div>
    <div class="form-group">
        <label>Выводить на главной</label>
        <select name="pos" class="form-control" value="{{old('pos', $category->pos)}}">
            <option value="">-</option>
            <option value="1" @if($category->pos == 1) selected @endif>Позиция 1</option>
            <option value="2" @if($category->pos == 2) selected @endif>Позиция 2</option>
        </select>
    </div>
    <nav>
        <div class="nav nav-tabs" role="tablist">
            @foreach(config('translatable.available_locale') as $key => $locale)
            <a class="nav-item nav-link @if(!$key) active @endif" data-toggle="tab" href="#{{$locale}}" role="tab">{{$locale}}</a>
            @endforeach
        </div>
    </nav>
    <div class="tab-content pt-3">
        @foreach(config('translatable.available_locale') as $key => $locale)
            <?php $category->setLocale($locale); ?>
            <div class="tab-pane fade @if(!$key) show active @endif" id="{{$locale}}" role="tabpanel">
                <div class="form-group">
                    <label>Название:</label>
                    <input type="text" class="form-control form-control-sm" value="{{old("name_$locale") ? old("name_$locale") : $category->name}}" name="name_{{$locale}}">
                </div>
            </div>
        @endforeach
    </div>
    <div class="form-group">
        <button class="btn btn-sm btn-success"><i class="fa fa-save"></i> Сохранить</button>
    </div>
</form>
@stop
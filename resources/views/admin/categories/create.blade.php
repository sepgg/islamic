@extends('layouts.admin', ['right' => false])

@section('content')
<div class="d-flex align-items-center justify-content-between">
    <span>Новая рубрика</span>
    <span><a href="{{route('admin.categories.index')}}" class="btn btn-sm btn-primary" data-toggle="tooltip" title="К списку"><i class="fa fa-arrow-left"></i></a></span>
</div>
<hr>
<form action="{{route('admin.categories.store')}}" method="POST">
    @csrf
    <div class="form-group form-check">
        <input name="active" type="checkbox" class="form-check-input" id="active">
        <label class="form-check-label" for="active">Активна</label>
    </div>
    <div class="form-group form-check">
        <input name="menu" type="checkbox" class="form-check-input" id="menu" @if(old('menu')) checked @endif>
        <label class="form-check-label" for="menu">В меню</label>
    </div>
    <div class="form-group">
        <label>Выводить на главной</label>
        <select name="pos" class="form-control">
            <option value="">-</option>
            <option value="1">Позиция 1</option>
            <option value="2">Позиция 2</option>
        </select>
    </div>
    <nav>
        <div class="nav nav-tabs" role="tablist">
            @foreach(config('translatable.available_locale') as $key => $locale)
            <a class="nav-item nav-link @if(!$key) active @endif" data-toggle="tab" href="#{{$locale}}" role="tab">{{$locale}}</a>
            @endforeach
        </div>
    </nav>
    <div class="tab-content pt-3">
        @foreach(config('translatable.available_locale') as $key => $locale)
            <div class="tab-pane fade @if(!$key) show active @endif" id="{{$locale}}" role="tabpanel">
                <div class="form-group">
                    <label>Название:</label>
                    <input type="text" class="form-control form-control-sm" name="name_{{$locale}}" value="{{old("name_$locale")}}">
                </div>
            </div>
        @endforeach
    </div>
    <div class="form-group">
        <button class="btn btn-sm btn-success"><i class="fa fa-save"></i> Сохранить</button>
    </div>
</form>
@stop
@extends('layouts.admin', ['right' => false])

@section('content')
<div class="d-flex align-items-center justify-content-between">
    <span>Список категорий</span>
    <span><a href="{{route('admin.categories.create')}}" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Добавить категорию</a></span>
</div>
@if($categories->count())
{{$categories->links()}}
<div class="table-tesponsive">
    <table class="table table-sm table-striped table-hover table-bordered">
        <tr>
            <th>ID</th>
            <th>Название</th>
            <th>Активна</th>
            <th>В меню</th>
            <th>Положение</th>
            <th width="100px">
        </tr>
        @foreach($categories as $category)
            <tr>
                <td width="100px" class="font-bold">{{$category->id}}</td>
                <td>{{$category->name}}</td>
                <td>@if($category->active) <i class="fa fa-check text-success"></i> @else <i class="fa fa-close text-danger"></i> @endif</td>
                <td>@if($category->menu) <i class="fa fa-check text-success"></i> @else <i class="fa fa-close text-danger"></i> @endif</td>
                <td>@if($category->pos) Позиция {{$category->pos}} @endif</td>
                <td width="150px">
                    <a href="{{route('admin.categories.edit', $category->id)}}" class="btn btn-sm btn-primary" data-toggle="tooltip" title="Редактировать"><i class="fa fa-pencil"></i></a>
                    <form action="{{route('admin.categories.destroy', $category->id)}}" class="d-inline-block confirmed" method="post">
                        @csrf
                        @method('delete')
                        <button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>
</div>
{{$categories->links()}}
@else
<p>Записей не найдено</p>
@endif
@stop
@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col">Редактирование пользователя</div>
    <div class="col text-right"><a href="{{route('admin.users.index')}}" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left"></i></a></div>
</div>
<hr>
<form action="{{route('admin.users.update', $user->id)}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="row">
        <div class="col-md-6">
            <div class="form-group form-check">
                <input name="active" type="checkbox" class="form-check-input" id="active" @if($user->active) checked @endif>
                <label class="form-check-label" for="active">Активен</label>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group form-check">
                <input name="manager" type="checkbox" class="form-check-input" id="manager" @if($user->manager) checked @endif>
                <label class="form-check-label" for="manager">Менеджер</label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Имя</label>
                <input type="text" class="form-control form-control-sm" name="name" value="{{old('name', $user->name)}}">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>E-mail</label>
                <input type="email" class="form-control form-control-sm" name="email" value="{{old('email', $user->email)}}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Пароль</label>
                <input type="password" class="form-control form-control-sm" name="password">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Подтвердите пароль</label>
                <input type="password" class="form-control form-control-sm" name="password_confirmation">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="d-block">Фото</label>
                <input type="file" name="photo">
            </div>
        </div>
        <div class="col-md-6">
            @if($user->photo)
            <img src="/storage/{{$user->photo}}" alt="{{$user->name}}" class="img-fluid">
            @endif
        </div>
    </div>
    <div class="form-group">
        <button class="btn btn-sm btn-success">Сохранить</button>
    </div>
</form>
@stop
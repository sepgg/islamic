@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col">Новый пользователь</div>
    <div class="col text-right"><a href="{{route('admin.users.index')}}" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left"></i></a></div>
</div>
<hr>
<form action="{{route('admin.users.store')}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-md-6">
            <div class="form-group form-check">
                <input name="active" type="checkbox" class="form-check-input" id="active" @if(old('active')) checked @endif>
                <label class="form-check-label" for="active">Активен</label>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group form-check">
                <input name="manager" type="checkbox" class="form-check-input" id="manager" @if(old('manager')) checked @endif>
                <label class="form-check-label" for="manager">Менеджер</label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Имя</label>
                <input type="text" class="form-control form-control-sm @error('name') is-invalid @enderror" name="name" value="{{old('name')}}">
                @error('name') <span class="invalid-feedback">{{$message}}</span> @enderror
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>E-mail</label>
                <input type="email" class="form-control form-control-sm @error('email') is-invalid @enderror" name="email" value="{{old('email')}}">
                @error('email') <span class="invalid-feedback">{{$message}}</span> @enderror
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Пароль</label>
                <input type="password" class="form-control form-control-sm @error('password') is-invalid @enderror" name="password">
                @error('password') <span class="invalid-feedback">{{$message}}</span> @enderror
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Подтвердите пароль</label>
                <input type="password" class="form-control form-control-sm" name="password_confirmation">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="d-block">Фото</label>
                <input type="file" name="photo">
            </div>
        </div>
    </div>
    <div class="form-group">
        <button class="btn btn-sm btn-success">Сохранить</button>
    </div>
</form>
@stop
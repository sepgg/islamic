@extends('layouts.admin')

@section('content')
<div class="d-flex align-items-center justify-content-between">
    <span>Список слайдов</span>
    <span><a href="{{route('admin.slides.create')}}" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Добавить слайд</a></span>
</div>
{{$slides->links()}}
<table class="table">
    <tr>
        <th>Заголовок</th>
        <th width="100px">Действия</th>
    </tr>
    @foreach($slides as $item)
    <tr>
        <td>{{$item->name}}</td>
        <td>
            <a href="{{route('admin.slides.edit', $item->id)}}" class="btn btn-sm btn-info"><i class="fa fa-pencil"></i></a>
            <form action="{{route('admin.slides.destroy', $item->id)}}" class="d-inline-block confirmed" method="post">
                @csrf
                @method('delete')
                <button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
            </form>
        </td>
    </tr>
    @endforeach
</table>
{{$slides->links()}}
@stop
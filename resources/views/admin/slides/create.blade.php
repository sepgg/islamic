@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col">Новый слайд</div>
    <div class="col text-right"><a href="{{route('admin.slides.index')}}" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left"></i></a></div>
</div>
<hr>
    <form action="{{route('admin.slides.store')}}" method="post" enctype="multipart/form-data">
        @csrf
        <nav>
            <div class="nav nav-tabs" role="tablist">
                @foreach(config('translatable.available_locale') as $key => $locale)
                <a class="nav-item nav-link @if(!$key) active @endif" data-toggle="tab" href="#{{$locale}}" role="tab">{{$locale}}</a>
                @endforeach
            </div>
        </nav>
        <div class="tab-content pt-3">
            @foreach(config('translatable.available_locale') as $key => $locale)
                <div class="tab-pane fade @if(!$key) show active @endif" id="{{$locale}}" role="tabpanel">
                    <div class="form-group">
                        <label>Заголовок</label>
                        <input type="text" class="form-control" name="name_{{$locale}}" value="{{old("name_$locale")}}">
                    </div>
                </div>
            @endforeach
        </div>
        <div class="form-group">
            <label>Картинка <input type="file" name="image"></label>
        </div>
        <div class="form-group">
            <button class="btn btn-primary">Сохранить</button>
        </div>
    </form>
@stop
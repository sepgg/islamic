@extends('layouts.admin')

@section('content')
<div class="d-flex align-items-center justify-content-between">
    <span>Список книг</span>
    <span><a href="{{route('admin.books.create')}}" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Добавить книгу</a></span>
</div>
{{$books->links()}}
<table class="table">
    <tr>
        <th>Название</th>
        <th>Цена</th>
        <th width="100px">Действия</th>
    </tr>
    @foreach($books as $item)
    <tr>
        <td>{{$item->name}}</td>
        <td>{{$item->price}}</td>
        <td>
            <a href="{{route('admin.books.edit', $item->id)}}" class="btn btn-info btn-sm"><i class="fa fa-pencil"></i></a>
            <form action="{{route('admin.books.destroy', $item->id)}}" class="d-inline-block confirmed" method="post">
                @csrf
                <input type="hidden" name="_method" value="DELETE">
                <button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
            </form>
        </td>
    </tr>
    @endforeach
</table>
{{$books->links()}}
@stop
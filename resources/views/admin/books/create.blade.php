@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col">Новая книга</div>
    <div class="col text-right"><a href="{{route('admin.books.index')}}" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left"></i></a></div>
</div>
<hr>
<form action="{{route('admin.books.store')}}" method="post" enctype="multipart/form-data">
    @csrf
    <nav>
        <div class="nav nav-tabs" role="tablist">
            @foreach(config('translatable.available_locale') as $key => $locale)
            <a class="nav-item nav-link @if(!$key) active @endif" data-toggle="tab" href="#{{$locale}}" role="tab">{{$locale}}</a>
            @endforeach
        </div>
    </nav>
    <div class="tab-content pt-3">
        @foreach(config('translatable.available_locale') as $key => $locale)
            <div class="tab-pane fade @if(!$key) show active @endif" id="{{$locale}}" role="tabpanel">
                <div class="form-group">
                    <label>Название</label>
                    <input type="text" class="form-control" name="name_{{$locale}}" value="{{old("name_$locale")}}">
                </div>
                <div class="form-group">
                    <label>Описание</label>
                    <textarea name="text_{{$locale}}" rows="10" class="form-control editor">{{old("text_$locale")}}</textarea>
                </div>
            </div>
        @endforeach
    </div>
    <div class="form-group">
        <label>Цена</label>
        <input type="number" class="form-control @error("price") is-invalid @enderror" name="price" value="{{old("price")}}">
        @error("price")<span class="invalid-feedback" role="alert">{{ $message }}</span>@enderror
    </div>
    <div class="form-group">
        <label>Изображение <input type="file" name="image"></label>
    </div>
    <div class="form-group">
        <button class="btn btn-success">Сохранить</button>
    </div>
</form>
@stop
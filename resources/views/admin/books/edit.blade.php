@extends('layouts.admin')

@section('content')
    <form action="{{route('admin.books.update', $book->id)}}" method="post" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <nav>
            <div class="nav nav-tabs" role="tablist">
                @foreach(config('translatable.available_locale') as $key => $locale)
                <a class="nav-item nav-link @if(!$key) active @endif" data-toggle="tab" href="#{{$locale}}" role="tab">{{$locale}}</a>
                @endforeach
            </div>
        </nav>
        <div class="tab-content pt-3">
            @foreach(config('translatable.available_locale') as $key => $locale)
                <?php $book->setLocale($locale); ?>
                <div class="tab-pane fade @if(!$key) show active @endif" id="{{$locale}}" role="tabpanel">
                    <div class="form-group">
                        <label>Название</label>
                        <input type="text" class="form-control @error("name_$locale") is-invalid @enderror" name="name_{{$locale}}" value="{{old("name_$locale", $book->name)}}">
                        @error("name_$locale")<span class="invalid-feedback" role="alert">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <label>Описание</label>
                        <textarea name="text_{{$locale}}" rows="7" class="form-control editor @error("text_$locale") is-invalid @enderror">{{old("text_$locale", $book->text)}}</textarea>
                        @error("text_$locale")<span class="invalid-feedback" role="alert">{{ $message }}</span>@enderror
                    </div>
                </div>
            @endforeach
        </div>
        <div class="form-group">
            <label>Цена</label>
            <input type="тгьиук" class="form-control @error("price") is-invalid @enderror" name="price" value="{{old("price", $book->price)}}">
            @error("price")<span class="invalid-feedback" role="alert">{{ $message }}</span>@enderror
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group @error('image') is-invalid @enderror">
                    <label>Изображение <input type="file" name="image"></label>
                    @error('image')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            @if($book->image)
            <div class="col-md-6">
                <img src="/storage/{{$book->image}}" alt="{{$book->name}}" class="img-fluid">
            </div>
            @endif
        </div>
        <div class="form-group">
            <button class="btn btn-success">Сохранить</button>
        </div>
    </form>
@stop
@extends('layouts.site')

@section('content')
<header class="det-headers">
        <div class="det-headers__banner">
            <img src="/storage/{{$post->image}}" alt="{{$post->name}}">
            <div class="containers">
                <h3>
                    {{$post->name}}
                </h3>
            </div>
        </div>
    </header>

    <section class="det-content">
        <div class="containers">
            <div class="det-content__block">
                <div class="det-content__block-date">
                    <h3>{{$post->created_at->format('F d, Y')}}</h3>
                    <p>{{$post->created_at->format('H:i')}}</p>
                    <a href="{{route('categories.show', $post->category->slug)}}" class="det-content__block-date-link">{{$post->category->name}}</a>
                </div>
                <div class="det-content__block-add">{{$post->preview}}</div>
                <div class="det-content__block-title">{{$post->name}}</div>
                <div class="det-content__block-description">{!!$post->text!!}</div>
            </div>
            <div class="det-content__add">
                <div class="det-content__add-title">The Latest News</div>
                <div class="det-content__add-last">
                    @foreach($latest as $item)
                    <a href="{{route('posts.show', [$item->category->slug, $item->slug])}}" class="det-content__add-last-item">
                        <div class="det-content__add-last-item-date">
                            <h3>{{$item->created_at->format('F d, Y')}}</h3>
                            <p>{{$item->created_at->format('H:i')}}</p>
                        </div>
                        <div class="det-content__add-last-item-text">{{$item->preview}}</div>
                        <div class="det-content__add-last-item-line"></div>
                    </a>
                    @endforeach
                </div>
                <div class="det-content__add-title">Follow Us</div>
                <ul class="det-content__add-contact">
                    <li class="det-content__add-contact-item">
                        <svg width="10" height="18" viewBox="0 0 10 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.99739 2.98875H9.64064V0.12675C9.35714 0.08775 8.38214 0 7.24664 0C4.87739 0 3.25439 1.49025 3.25439 4.22925V6.75H0.639893V9.9495H3.25439V18H6.45989V9.95025H8.96864L9.36689 6.75075H6.45914V4.5465C6.45989 3.62175 6.70889 2.98875 7.99739 2.98875Z" fill="black"/></svg>
                        <div class="det-content__add-contact-item-line"></div>
                        <div class="det-content__add-contact-item-text">
                            <p>134 000</p>
                            <h3>Followers</h3>
                        </div>
                    </li>
                    <li class="det-content__add-contact-item">
                        <svg width="18" height="16" viewBox="0 0 18 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M18 2.41887C17.3306 2.7125 16.6174 2.90712 15.8737 3.00162C16.6388 2.54487 17.2226 1.82712 17.4971 0.962C16.7839 1.38725 15.9964 1.68763 15.1571 1.85525C14.4799 1.13413 13.5146 0.6875 12.4616 0.6875C10.4186 0.6875 8.77387 2.34575 8.77387 4.37863C8.77387 4.67113 8.79862 4.95237 8.85938 5.22012C5.7915 5.0705 3.07687 3.60013 1.25325 1.36025C0.934875 1.91263 0.748125 2.54488 0.748125 3.2255C0.748125 4.5035 1.40625 5.63637 2.38725 6.29225C1.79437 6.281 1.21275 6.10888 0.72 5.83775C0.72 5.849 0.72 5.86363 0.72 5.87825C0.72 7.6715 1.99912 9.161 3.6765 9.50412C3.37612 9.58625 3.04875 9.62562 2.709 9.62562C2.47275 9.62562 2.23425 9.61213 2.01038 9.56262C2.4885 11.024 3.84525 12.0984 5.4585 12.1332C4.203 13.1154 2.60888 13.7071 0.883125 13.7071C0.5805 13.7071 0.29025 13.6936 0 13.6565C1.63462 14.7106 3.57188 15.3125 5.661 15.3125C12.4515 15.3125 16.164 9.6875 16.164 4.81175C16.164 4.64862 16.1584 4.49113 16.1505 4.33475C16.8829 3.815 17.4982 3.16587 18 2.41887Z" fill="black"/></svg>
                        <div class="det-content__add-contact-item-line"></div>
                        <div class="det-content__add-contact-item-text">
                            <p>134 000</p>
                            <h3>Followers</h3>
                        </div>
                    </li>
                    <li class="det-content__add-contact-item">
                        <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M13.1275 0H4.87243C2.18573 0 0 2.18573 0 4.87243V13.1277C0 15.8142 2.18573 18 4.87243 18H13.1277C15.8142 18 18 15.8142 18 13.1277V4.87243C18 2.18573 15.8142 0 13.1275 0V0ZM16.9447 13.1277C16.9447 15.2324 15.2324 16.9447 13.1275 16.9447H4.87243C2.76759 16.9447 1.05523 15.2324 1.05523 13.1277V4.87243C1.05523 2.76759 2.76759 1.05523 4.87243 1.05523H13.1277C15.2324 1.05523 16.9447 2.76759 16.9447 4.87243V13.1277Z" fill="black"/><path d="M9.00059 4.07812C6.28669 4.07812 4.07886 6.28596 4.07886 8.99985C4.07886 11.7137 6.28669 13.9216 9.00059 13.9216C11.7145 13.9216 13.9223 11.7137 13.9223 8.99985C13.9223 6.28596 11.7145 4.07812 9.00059 4.07812ZM9.00059 12.8663C6.86869 12.8663 5.13409 11.1319 5.13409 8.99985C5.13409 6.86796 6.86869 5.13336 9.00059 5.13336C11.1326 5.13336 12.8671 6.86796 12.8671 8.99985C12.8671 11.1319 11.1326 12.8663 9.00059 12.8663Z" fill="black"/><path d="M14.039 2.33032C13.237 2.33032 12.5847 2.98277 12.5847 3.78463C12.5847 4.58664 13.237 5.23908 14.039 5.23908C14.841 5.23908 15.4935 4.58664 15.4935 3.78463C15.4935 2.98263 14.841 2.33032 14.039 2.33032ZM14.039 4.18371C13.819 4.18371 13.64 4.00464 13.64 3.78463C13.64 3.5645 13.819 3.38556 14.039 3.38556C14.2592 3.38556 14.4382 3.5645 14.4382 3.78463C14.4382 4.00464 14.2592 4.18371 14.039 4.18371Z" fill="black"/></svg>
                        <div class="det-content__add-contact-item-line"></div>
                        <div class="det-content__add-contact-item-text">
                            <p>134 000</p>
                            <h3>Followers</h3>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <section class="det-message">
        <div class="containers">
            <div class="det-message__title">Comment</div>
            <form action="{{route('posts.comment', $post->id)}}" class="det-message__block" method="post">
                @csrf
                <div class="det-message__block-photo"></div>
                <textarea class="det-message__block-textarea" name="text" placeholder="Write your comment in here"></textarea>
                <button class="det-message__block-btn">Post Now</button>
            </form>
            <ul class="det-message__list">
                @foreach($post->comments as $item)
                <li class="det-message__list-item">
                    <div class="det-message__list-item-photo"></div>
                    <div class="det-message__list-item-client">
                        <div class="det-message__list-item-client-name">{{$item->user->name ?? __('comments.guest')}}</div>
                        <div class="det-message__list-item-client-date">{{$item->created_at->diffForHumans()}}</div>
                    </div>
                    <div class="det-message__list-item-messages">
                        {{$item->text}}
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
    </section>
@stop
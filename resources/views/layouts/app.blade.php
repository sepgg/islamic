<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Панель администратора</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="/vendor/tinymce/tinymce.min.js"></script>
    <script src="/vendor/tinymce/langs/ru.js"></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <div class="container-fluid px-0">
            <div class="row no-gutters w-100">
                <div class="col sidebar">
                    <div class="border-right border-left">
                        <div class="media d-flex align-items-center px-3 border-bottom py-1 position-relative">
                            @if(auth()->user()->photo)
                                <img width="53px" src="/storage/{{auth()->user()->photo}}" class="mr-3" alt="{{auth()->user()->name}}">
                            @else
                                <img width="53px" src="/img/nophoto.png" class="mr-3" alt="{{auth()->user()->name}}">
                            @endif
                            <div class="media-body">
                                <p class="font-weight-bolder mb-0">{{auth()->user()->name}}</p>
                                <small class="text-muted text-uppercase">{{auth()->user()->email}}</small>
                            </div>
                            <button class="btn btn-sm btn-outline-secondary sidebar-toggle d-inline-block d-lg-none "><i class="fa fa-close"></i></button>
                        </div>
                        <div class="list-group border-0">
                            
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="border-bottom shadow-sm px-3" style="padding-top:14px;padding-bottom:14px">
                        <div class="d-flex justify-content-between align-items-center w-100">
                            <span class="text-uppercase font-weight-bolder d-none d-md-inline-block">Личный кабинет</span>
                            <div>
                                <a href="{{route('home')}}" class="btn btn-sm btn-primary"><i class="fa fa-home"></i></a>
                                @if(auth()->user()->admin || auth()->user()->manager)
                                    <a href="{{route('admin.index')}}" data-toggle="tooltip" data-title="Панель администратора" class="btn btn-sm btn-danger"><i class="fa fa-lock"></i></a>
                                @endif
                                <a href="{{ route('logout') }}" class="btn btn-sm btn-warning" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-power-off"></i></a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                                <button class="btn btn-sm btn-outline-secondary sidebar-toggle d-inline-block d-lg-none "><i class="fa fa-bars"></i></button>
                            </div>
                        </div>
                    </div>
                    <main class="p-3">
                        <div>
                            @if(session()->has('success'))
                            <p class="text-success">{{session()->get('success')}}</p>
                            @elseif(session()->has('error'))
                            <p class="text-error">{{session()->get('error')}}</p>
                            @endif
                            @yield('content')
                        </div>
                    </main>
                </div>
            </div>
        </div>
    </div>
</body>
</html>

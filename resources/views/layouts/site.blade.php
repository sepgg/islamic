<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Islamic</title>
    <link rel="stylesheet" href="https://necolas.github.io/normalize.css/8.0.1/normalize.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
    <link rel="stylesheet" href="/css/style.min.css">
    <link rel="stylesheet" href="/css/custom.css">
</head>
<body @if($lang == 'ar') dir="rtl" @endif>
    <div id="app">
        <section class="ind-header">
            <div class="containers">
                <div class="ind-header__nav">
                    <a href="{{route('home')}}" class="ind-header__nav-logo">{{__('ui.title')}}</a>
                    <ul class="ind-header__nav-list">
                        <div class="ind-header__nav-item">
                            <div class="language-select-item">
                                <div class="language-select-placeholder">{{$langs[$lang]}}</div>
                                <div id="languageSelectorHeader" class="language-select-wrapper display-n" style="width: auto">
                                    @foreach($langs as $key => $item)
                                    <a href="{{route('lang', $key)}}" class="language-select-element">{{$item}}</a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="ind-header__nav-item">
                            <basket-icon></basket-icon>
                            <a href="{{route('search')}}" class="ind-header__nav-item-search"><svg width="17" height="17" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M16.4572 15.4843L12.4111 11.2762C13.4514 10.0396 14.0214 8.48359 14.0214 6.86374C14.0214 3.07913 10.9423 0 7.15769 0C3.37308 0 0.293945 3.07913 0.293945 6.86374C0.293945 10.6484 3.37308 13.7275 7.15769 13.7275C8.57848 13.7275 9.93243 13.299 11.09 12.4854L15.1668 16.7255C15.3372 16.9024 15.5664 17 15.812 17C16.0444 17 16.265 16.9114 16.4324 16.7502C16.7881 16.4079 16.7995 15.8403 16.4572 15.4843ZM7.15769 1.79054C9.95511 1.79054 12.2309 4.06632 12.2309 6.86374C12.2309 9.66117 9.95511 11.9369 7.15769 11.9369C4.36027 11.9369 2.08449 9.66117 2.08449 6.86374C2.08449 4.06632 4.36027 1.79054 7.15769 1.79054Z" fill="#5EA956"/></svg></a>
                            <a href="{{route('cabinet.index')}}" class="ind-header__nav-item-client" id="client-open"><svg width="18" height="18" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M27.6203 19.0437C26.6261 18.17 25.4203 17.4152 24.0363 16.8002C23.4446 16.5374 22.7522 16.8038 22.4895 17.3952C22.2267 17.9867 22.4931 18.679 23.0846 18.942C24.2516 19.4607 25.2573 20.0871 26.0733 20.8042C27.0792 21.6882 27.6562 22.9681 27.6562 24.3164V26.4844C27.6562 27.1305 27.1305 27.6562 26.4844 27.6562H3.51562C2.86949 27.6562 2.34375 27.1305 2.34375 26.4844V24.3164C2.34375 22.9681 2.92076 21.6882 3.9267 20.8042C5.1107 19.7637 8.56041 17.3438 15 17.3438C19.7816 17.3438 23.6719 13.4534 23.6719 8.67188C23.6719 3.8903 19.7816 0 15 0C10.2184 0 6.32812 3.8903 6.32812 8.67188C6.32812 11.4672 7.65793 13.9574 9.71786 15.5443C5.94864 16.3728 3.60397 17.9677 2.37968 19.0437C0.867462 20.3723 0 22.294 0 24.3164V26.4844C0 28.423 1.577 30 3.51562 30H26.4844C28.423 30 30 28.423 30 26.4844V24.3164C30 22.294 29.1325 20.3723 27.6203 19.0437ZM8.67188 8.67188C8.67188 5.18257 11.5107 2.34375 15 2.34375C18.4893 2.34375 21.3281 5.18257 21.3281 8.67188C21.3281 12.1612 18.4893 15 15 15C11.5107 15 8.67188 12.1612 8.67188 8.67188Z" fill="black"/></svg></a>
                            <div class="ind-header__nav-item-menu" id="menu-open"><svg width="34" height="20" viewBox="0 0 34 20" fill="none" xmlns="http://www.w3.org/2000/svg"><line x1="1" y1="1" x2="33" y2="1" stroke="black" stroke-width="2" stroke-linecap="round"/>						<line x1="1" y1="10" x2="33" y2="10" stroke="black" stroke-width="2" stroke-linecap="round"/><line x1="1" y1="19" x2="33" y2="19" stroke="black" stroke-width="2" stroke-linecap="round"/></svg></div>
                            <div class="ind-header__nav-item-close display-n" id="menu-close"><svg width="31" height="31" viewBox="0 0 31 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M23.25 7.75L7.75 23.25" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/><path d="M7.75 7.75L23.25 23.25" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg></div>
                        </div>
                    </ul>
                </div>
                <div class="ind-header__line"></div>
                <div class="ind-header__content display-n" id="menu">
                    <div class="ind-header__content-item"><a href="{{route('home')}}">{{__('ui.home')}}</a></div>
                    @foreach($categories as $item)
                    <div class="ind-header__content-item"><a href="{{route('categories.show', $item->slug)}}">{{$item->name}}</a></div>
                    @endforeach
                    <div class="ind-header__content-item"><a href="{{route('books.index')}}">{{__('ui.books')}}</a></div>
                </div>
            </div>
        </section>
        <div class="ind-header__media display-n" id="menuMedia">
            <div class="ind-header__media-nav">
                <div class="ind-header__media-nav-title">{{__('ui.title')}}</div>
                <div class="ind-header__media-nav-icons">
                    <div class="ind-header__media-nav-icons-basket">
                        <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M6.5 15.1579C6.20333 15.1579 5.91332 15.2412 5.66664 15.3974C5.41997 15.5535 5.22771 15.7755 5.11418 16.0351C5.00065 16.2948 4.97094 16.5805 5.02882 16.8562C5.0867 17.1318 5.22956 17.385 5.43934 17.5838C5.64912 17.7825 5.91639 17.9179 6.20736 17.9727C6.49834 18.0275 6.79994 17.9994 7.07403 17.8918C7.34811 17.7843 7.58238 17.6021 7.7472 17.3684C7.91203 17.1348 8 16.86 8 16.5789C8 16.2021 7.84196 15.8406 7.56066 15.5741C7.27936 15.3076 6.89782 15.1579 6.5 15.1579ZM17 12.3158H5C4.73478 12.3158 4.48043 12.216 4.29289 12.0383C4.10536 11.8606 4 11.6197 4 11.3684C4 11.1172 4.10536 10.8762 4.29289 10.6985C4.48043 10.5209 4.73478 10.4211 5 10.4211H13.4912C14.1426 10.4191 14.7758 10.2171 15.2959 9.84552C15.816 9.4739 16.195 8.95262 16.376 8.35979L17.9614 3.10254C18.004 2.96154 18.0114 2.81312 17.9831 2.66895C17.9548 2.52478 17.8915 2.3888 17.7983 2.27172C17.7051 2.15464 17.5845 2.05966 17.446 1.99424C17.3075 1.92882 17.1548 1.89476 17 1.89474H4.73907C4.53206 1.34276 4.15044 0.864672 3.64622 0.525613C3.14201 0.186554 2.53971 0.00301334 1.92139 0H1C0.734784 0 0.48043 0.0998117 0.292893 0.277478C0.105357 0.455144 0 0.696111 0 0.947368C0 1.19863 0.105357 1.43959 0.292893 1.61726C0.48043 1.79492 0.734784 1.89474 1 1.89474H1.92139C2.13847 1.89546 2.34947 1.96277 2.52279 2.08661C2.6961 2.21044 2.82242 2.38413 2.88281 2.58167L3.03833 3.09774L3.03857 3.10254L4.6792 8.54251C3.91576 8.62031 3.21287 8.97247 2.71527 9.52648C2.21766 10.0805 1.96321 10.7942 2.00433 11.5205C2.04544 12.2469 2.37898 12.9307 2.93626 13.4312C3.49355 13.9316 4.23216 14.2105 5 14.2105H17C17.2652 14.2105 17.5196 14.1107 17.7071 13.933C17.8946 13.7554 18 13.5144 18 13.2632C18 13.0119 17.8946 12.7709 17.7071 12.5933C17.5196 12.4156 17.2652 12.3158 17 12.3158ZM15.6743 3.78947L14.4531 7.83892C14.3928 8.03662 14.2664 8.21047 14.093 8.3344C13.9196 8.45833 13.7084 8.52567 13.4912 8.52632H6.75439L6.49945 7.68106L5.32642 3.78947H15.6743ZM14.5 15.1579C14.2033 15.1579 13.9133 15.2412 13.6666 15.3974C13.42 15.5535 13.2277 15.7755 13.1142 16.0351C13.0006 16.2948 12.9709 16.5805 13.0288 16.8562C13.0867 17.1318 13.2296 17.385 13.4393 17.5838C13.6491 17.7825 13.9164 17.9179 14.2074 17.9727C14.4983 18.0275 14.7999 17.9994 15.074 17.8918C15.3481 17.7843 15.5824 17.6021 15.7472 17.3684C15.912 17.1348 16 16.86 16 16.5789C16 16.2021 15.842 15.8406 15.5607 15.5741C15.2794 15.3076 14.8978 15.1579 14.5 15.1579Z" fill="black"/></svg>
                        <div class="ind-header__media-nav-icons-basket-count">1</div>
                    </div>
                    <div class="ind-header__media-nav-icons-close" id="menu-close-media"><svg width="31" height="31" viewBox="0 0 31 31" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M23.25 7.75L7.75 23.25" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/><path d="M7.75 7.75L23.25 23.25" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg></div>
                </div>
            </div>
            <div class="ind-header__media-table">
                <div class="ind-header__media-item"><a href="{{route('home')}}">{{__('ui.home')}}</a></div>
                @foreach($categories as $item)
                    <div class="ind-header__media-item"><a href="{{route('categories.show', $item->slug)}}">{{$item->name}}</a></div>
                @endforeach
                <div class="ind-header__media-item"><a href="{{route('books.index')}}">{{__('ui.books')}}</a></div>
            </div>
        </div>

        @yield('content')
        <footer class="footer">
            <div class="containers">
                <div class="footer__title">{{__('ui.title')}}</div>
                <div class="footer__block">
                    <div class="footer__block-contact">
                        <div class="footer__block-contact-title">{{__('ui.subscribe.title')}}</div>
                        <form action="{{route('subscribe')}}" method="post">
                            @csrf
                            <div class="footer__block-contact-add">
                                <div class="footer__block-contact-add-text">{{__('ui.subscribe.placeholder')}}</div>
                                <button class="footer__block-contact-add-btn"><svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M3.77783 2L18.0001 2V16.2222" stroke="white" stroke-width="3"/><path d="M18 2L2 18" stroke="white" stroke-width="3"/></svg></button>
                            </div>
                            <input type="text" class="footer__block-contact-input" name="email">
                        </form>
                    </div>
                    <div class="footer__block-list">
                        <div class="footer__block-list-title">{{__('ui.categories')}}</div>
                        @foreach($categories as $item)
                        <a href="{{route('categories.show', $item->slug)}}" class="footer__block-list-item">{{$item->name}}</a>
                        @endforeach
                    </div>
                    <ul class="footer__block-list">
                        <div class="footer__block-list-title">{{__('ui.shop')}}</div>
                    </ul>
                    <ul class="footer__block-list">
                        <li class="footer__block-list-title">Facebook</li>
                        <li class="footer__block-list-title">Instagram</li>
                        <li class="footer__block-list-title">Linkedin</li>
                        <li class="footer__block-list-title">Pinterest</li>
                    </ul>
                </div>
                <div class="footer__add">
                    <div class="footer__add-rights">© 2021 {{__('ui.rights')}}</div>
                </div>
            </div>
        </footer>
        <div class="basket">
            <div class="basket__title">
                <div class="basket__title-item">Your cart</div>
                <a href="#" class="basket__title-close"><svg width="19" height="19" viewBox="0 0 19 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 17.5L17.5 1" stroke="#AFB1BD" stroke-width="2"/><path d="M17.5 17.5L0.999999 1" stroke="#AFB1BD" stroke-width="2"/></svg></a>
            </div>
            <div class="basket__line"></div>
            <basket lang="{{$lang}}" total="{{__('ui.basket.total')}}"></basket>
            <div class="basket__line"></div>
            <a href="{{route('basket.index')}}" class="basket__btn">Checkout</a>
        </div>
        @if(session()->has('success') || session()->has('error') )
        <div class="modelLogin">
            <div class="modelLogin__regs">
                <div class="modelLogin__regs-close close-toggle">
                    <img src="/svg/close-client.svg" alt="">
                </div>
                <div class="modelLogin__regs-title">
                    {{__(session()->has('success') ? session()->get('success') : session()->get('error'))}}
                </div>
            </div>
        </div>
        @endif
    </div>
    <script src="/js/app.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TweenMax.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
    <script src="https://cdn.rawgit.com/matthieua/WOW/1.0.1/dist/wow.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/easy-pie-chart/2.1.6/jquery.easypiechart.min.js"></script>
    <script src="/js/script.js"></script>
</body>
</html>

@extends('layouts.site')
@section('content')
<header class="card-headers">
        <div class="containers">
            <!--<div class="card-headers__title">Catalog  /  Product Name</div>-->
            <div class="card-headers__block">
                <div class="card-headers__block-photo">
                    <div class="card-headers__block-photo-element"><img style="display: inline-block;max-width: 100%" src="/storage/{{$book->image}}" alt="{{$book->name}}"></div>
                    <div class="card-headers__block-photo-list">
                        <div class="card-headers__block-photo-item">
                            <div class="card-headers__block-photo-item-circle">
                                <p>FREE</p>
                            </div>
                            <div class="card-headers__block-photo-item-text">
                                Benefits <br>
                                description
                            </div>
                        </div>
                        <div class="card-headers__block-photo-item">
                            <div class="card-headers__block-photo-item-circle">
                                <p>FREE</p>
                            </div>
                            <div class="card-headers__block-photo-item-text">
                                Benefits <br>
                                description
                            </div>
                        </div>
                        <div class="card-headers__block-photo-item">
                            <div class="card-headers__block-photo-item-circle">
                                <p>FREE</p>
                            </div>
                            <div class="card-headers__block-photo-item-text">
                                Benefits <br>
                                description
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-headers__block-controls">
                    <div class="card-headers__block-controls-title">{{$book->name}}</div>
                    <div class="card-headers__block-controls-price">
                        <div class="card-headers__block-controls-price-last">{{$book->price}}$</div>
                        <div class="card-headers__block-controls-price-prev"></div>
                    </div>
                    <p>{!!$book->text!!}</p>
                    <div class="card-headers__block-controls-line"></div>
                    <div class="card-headers__block-controls-btns">
                        <button class="card-headers__block-controls-btns-btn addtocart" data-id="{{$book->id}}">Add to Basket</button>
                        <button class="card-headers__block-controls-btns-basket"><svg width="17" height="18" viewBox="0 0 17 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M2.98421 2.66663L2.71575 1.51331C2.50676 0.623727 1.7579 -5.25233e-05 0.899008 3.31708e-09H0V1.33331H0.899008C1.19287 1.32417 1.45289 1.53519 1.52332 1.83997L3.74587 11.4932L3.97686 12.4865C4.18752 13.3833 4.94647 14.0091 5.81234 13.9998H14.3592V12.6665H5.81234C5.51848 12.6756 5.25846 12.4646 5.18803 12.1598L5.11311 11.8265L14.3592 9.49986C15.0427 9.33017 15.5801 8.76664 15.7514 8.03988L17 2.66663H2.98421ZM14.5652 7.71989C14.5074 7.96334 14.3264 8.15146 14.097 8.20655L4.82593 10.5332L3.29636 3.99994H15.433L14.5652 7.71989ZM3 16C3 14.8954 3.89543 14 5 14C6.10457 14 7 14.8954 7 16C7 17.1046 6.10457 18 5 18C3.89543 18 3 17.1046 3 16ZM4.33333 16C4.33333 16.3682 4.63181 16.6667 5 16.6667C5.36819 16.6667 5.66667 16.3682 5.66667 16C5.66667 15.6318 5.36819 15.3333 5 15.3333C4.63181 15.3333 4.33333 15.6318 4.33333 16ZM10.1523 15.2346C10.4619 14.4873 11.1911 14 12 14C13.1046 14 14 14.8954 14 16C14 16.8089 13.5127 17.5381 12.7654 17.8477C12.018 18.1573 11.1578 17.9862 10.5858 17.4142C10.0138 16.8422 9.84274 15.982 10.1523 15.2346ZM11.3334 16C11.3334 16.3682 11.6318 16.6666 12 16.6666C12.3682 16.6666 12.6667 16.3682 12.6667 16C12.6667 15.6318 12.3682 15.3333 12 15.3333C11.6318 15.3333 11.3334 15.6318 11.3334 16Z" fill="black"/></svg></button>
                    </div>
                </div>
            </div>
            @if($books->count())
            <div class="card-headers__add">
                <div class="card-headers__add-title">Title</div>
                <div class="card-headers__add-slider">
                    <div class="swiper cardElementSlider">
                        <div class="swiper-wrapper">
                            @foreach($books as $book)
                            <div class="swiper-slide">
                                <div class="card-headers__add-slider-item">
                                    <div class="card-headers__add-slider-item-photo"></div>
                                    <div class="card-headers__add-slider-item-title">Name</div>
                                    <div class="card-headers__add-slider-item-price">
                                        <div class="card-headers__add-slider-item-price-last">Price</div>
                                        <div class="card-headers__add-slider-item-price-prev">Price</div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="swiper-controls">
                        <div class="swiper-button-next">
                            <div class="swiper-button-next-arrow"><svg width="21" height="9" viewBox="0 0 21 9" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M15.1402 0.116733C15.3271 -0.0389109 15.6302 -0.0389109 15.8171 0.116733L20.8598 4.31579L20.916 4.37195C21.0467 4.53002 21.0246 4.74876 20.8517 4.88605L15.8089 8.88989L15.7413 8.93468C15.5518 9.03856 15.2944 9.01808 15.1322 8.87648L15.0784 8.8202C14.9537 8.66236 14.9783 8.44802 15.1483 8.31301L19.3211 5H0.976191L0.890594 4.99194C0.668451 4.94961 0.5 4.74546 0.5 4.5C0.5 4.22386 0.713198 4 0.976191 4H19.1277L15.1402 0.680368L15.0848 0.625171C14.9556 0.469841 14.974 0.255083 15.1402 0.116733Z" fill="black"/></svg></div>
                        </div>
                        <div class="swiper-button-prev">
                            <div class="swiper-button-next-arrow"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M7.85981 8.11673C7.6729 7.96109 7.36985 7.96109 7.18293 8.11673L2.14019 12.3158L2.084 12.3719C1.95329 12.53 1.97541 12.7488 2.14834 12.8861L7.19108 16.8899L7.25866 16.9347C7.44823 17.0386 7.70562 17.0181 7.86777 16.8765L7.92156 16.8202C8.04631 16.6624 8.02172 16.448 7.85166 16.313L3.67891 13H22.5238L22.6094 12.9919C22.8315 12.9496 23 12.7455 23 12.5C23 12.2239 22.7868 12 22.5238 12H3.87229L7.85981 8.68037L7.91519 8.62517C8.04442 8.46984 8.02596 8.25508 7.85981 8.11673Z" fill="#000"/></svg></div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </header>
@stop
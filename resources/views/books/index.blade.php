@extends('layouts.site')
@section('content')
<header class="catl-headers">
        <div class="containers">
            <div class="catl-headers__nav">
                <div class="catl-headers__nav-text-filter">
                    <div class="catl-headers__nav-text-filter-block">
                        <div class="catl-headers__nav-text-filter-title">Catalog</div>
                        <div class="catl-headers__nav-text-filter-icon">
                            <svg width="21" height="15" viewBox="0 0 21 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M8.16666 14.7002H12.8333V12.3669H8.16666V14.7002ZM0 0.700195V3.03353H21V0.700195H0ZM3.5 8.86686H17.5V6.53352H3.5V8.86686Z" fill="white"/></svg>
                        </div>
                    </div>
                    <div class="catl-headers__nav-text-filter-wrapper display-n">
                        <ul class="catl-headers__nav-text-filter-list">
                            <li class="catl-headers__nav-text-filter-item">
                                <div class="catl-headers__nav-text-filter-checkbox">
                                    <input class="catl-headers__nav-text-filter-checkbox__input" type="checkbox" id="checkbox_1">
                                    <label class="catl-headers__nav-text-filter-checkbox__label" for="checkbox_1">По возрастанию</label>
                                </div>
                            </li>
                            <li class="catl-headers__nav-text-filter-item">
                                <div class="catl-headers__nav-text-filter-checkbox">
                                    <input class="catl-headers__nav-text-filter-checkbox__input" type="checkbox" id="checkbox_2">
                                    <label class="catl-headers__nav-text-filter-checkbox__label" for="checkbox_2">По убыванию цены</label>
                                </div>
                            </li>
                        </ul>
                        <div class="catl-headers__nav-text-filter-range">
                            <div class="catl-headers__nav-text-filter-range-value-1">1690</div>
                            <div class="catl-headers__nav-text-filter-range-value-2">16090</div>
                            <input type="range" id="marchRange-1" value="1680" min="1680" max="16090" class="catl-headers__nav-text-filter-range-1">
                            <input type="range" id="marchRange-2" value="16090" min="1680" max="16090" class="catl-headers__nav-text-filter-range-2">
                            <div class="catl-headers__nav-text-filter-range-prev">от</div>
                            <div class="catl-headers__nav-text-filter-range-next">до</div>
                        </div>
                        <div class="catl-headers__nav-text-filter-discard"><p>Сбросить</p></div>
                    </div>
                </div>
            </div>
            <div class="catl-headers__content">
                @foreach($books as $book)
                <div class="catl-headers__content-item">
                    <div class="catl-headers__content-item-photo"><img src="/storage/{{$book->image}}" alt="{{$book->image}}"></div>
                    <div class="catl-headers__content-item-title">{{$book->name}}</div>
                    <div class="catl-headers__content-item-price">
                        <div class="catl-headers__content-item-price-last">Price</div>
                        <div class="catl-headers__content-item-price-prev">{{$book->price}}</div>
                    </div>
                    <a href="{{route('books.show', $book->id)}}" class="catl-headers__content-item-arrow">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M15.1867 6.28345C14.8087 6.66137 14.8087 7.27411 15.1867 7.65204L18.6959 11.1613H2.96774C2.43327 11.1613 2 11.5946 2 12.129C2 12.6635 2.43327 13.0968 2.96774 13.0968H18.6959L15.1867 16.606C14.8087 16.984 14.8087 17.5967 15.1867 17.9746C15.5646 18.3525 16.1773 18.3525 16.5553 17.9746L21.7162 12.8137M21.9266 11.7586C21.8793 11.6445 21.8093 11.5375 21.7166 11.4447L16.5553 6.28345C16.1773 5.90552 15.5646 5.90552 15.1867 6.28345M21.9266 11.7586C21.9735 11.8719 21.9996 11.996 22 12.1261C22 12.1271 22 12.1281 22 12.129C22 12.13 22 12.131 22 12.1319C21.9993 12.3777 21.9055 12.6233 21.7186 12.8113" fill="#2E2E2E"/></svg>
                    </a>
                </div>
                @endforeach
            </div>
        </div>
    </header>
@stop
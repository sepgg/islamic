@extends('layouts.site')

@section('content')

<header class="ind-headers">
        <div class="ind-headers__slider">
            <div class="swiper indexHeaderSlider">
                <div class="swiper-wrapper">
                    @foreach($slides as $slide)
                    <div class="swiper-slide">
                        <div class="ind-headers__slider-item">
                            <img src="/storage/{{$slide->image}}" alt="{{$slide->name}}">
                            <div class="containers">
                                <div class="ind-headers__slider-title">
                                    <p>{{$slide->name}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="swiper-controls">
                <div class="swiper-button-next">
                    <div class="swiper-button-next-arrow"></div>
                </div>
                <div class="swiper-button-prev">
                    <div class="swiper-button-prev-arrow"></div>
                </div>
            </div>
        </div>
    </header>
    @if($posts->count())
    <section class="ind-last">
        <div class="containers">
            <div class="ind-last__banner">
                <a href="{{route('posts.show', [$posts[0]->category->slug, $posts[0]->slug])}}">
                    <div class="ind-last__banner-title">{{$posts[0]->name}}</div>
                    <div class="ind-last__banner-photo">
                        <img src="/storage/{{$posts[0]->image}}" alt="{{$posts[0]->name}}">
                    </div>
                </a>
                <div class="ind-last__banner-text">{{$posts[0]->preview}}</div>
                <div class="ind-last__banner-add">
                    <p>{{__('ui.news')}}</p>
                    <div class="ind-last__banner-add-line"></div>
                </div>
            </div>
            <div class="ind-last__add">
                @foreach($posts as $key => $post)
                    @if($key && $key < 3)
                        <a href="{{route('posts.show', [$post->category->slug, $post->slug])}}">
                            <div class="ind-last__add-texts">{{$post->name}}</div>
                            <div class="ind-last__add-photo"><img src="/storage/{{$post->image}}" alt="{{$post->name}}"></div>
                        </a>
                    @endif
                @endforeach
            </div>
        </div>
    </section>
    @endif
    <section class="ind-news">
        <div class="ind-news__list">
            <div class="containers">
                @foreach($posts->splice(0, 3) as $post)
                <div class="ind-news__list-item">
                    <div class="ind-news__list-item-date">
                        <p>{{$post->created_at->format('F d, Y')}}</p>
                        <h3>{{$post->created_at->format('H:i')}}</h3>
                    </div>
                    <a href="{{route('posts.show', [$post->category->slug, $post->slug])}}" class="ind-news__list-item-text">
                        {{$post->name}}
                    </a>
                </div>
                @endforeach
            </div>
        </div>
        <div class="ind-news__block">
            <div class="containers">
                <div class="ind-news__block-slider">
                    <div class="swiper indexNewsSlider">
                        <div class="swiper-wrapper">
                            @foreach($posts as $post)
                            <div class="swiper-slide">
                                <a href="{{route('posts.show', [$post->category->slug, $post->slug])}}" class="ind-news__block-item">
                                    <div class="ind-news__block-item-title">{{$post->name}}</div>
                                    <div class="ind-news__block-item-text">{{$post->preview}}</div>
                                    <div class="ind-news__block-item-date">
                                        <h3>{{$post->created_at->format('F d, Y')}}</h3>
                                        <p>{{$post->created_at->format('H:i')}}</p>
                                    </div>
                                </a>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if($hot->count())
        <div class="ind-news__banner">
            <div class="containers">
                <div class="ind-news__banner-wrapper">
                    <div class="ind-news__banner-icons"><svg width="22" height="32" viewBox="0 0 22 32" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M5.83175 31.9996C2.27459 20.2782 9.79079 15.9984 9.79079 15.9984C9.26491 22.253 12.8193 27.1259 12.8193 27.1259C14.1265 26.7314 16.6217 24.8881 16.6217 24.8881C16.6217 27.1259 15.3051 31.9968 15.3051 31.9968C15.3051 31.9968 19.914 28.4331 21.3649 22.5141C22.8138 16.5951 18.6049 10.6529 18.6049 10.6529C18.8585 14.8422 17.4413 18.9625 14.6645 22.1104C14.8035 21.95 14.92 21.7719 15.0095 21.5789C15.5084 20.5812 16.3093 17.9882 15.8403 11.9834C15.1802 3.55437 7.54274 0 7.54274 0C8.20102 5.13575 6.2271 6.31898 1.60325 16.0674C-3.0206 25.8139 5.83175 31.9996 5.83175 31.9996Z" fill="white"/></svg></div>
                    <div class="ind-news__banner-add">{{$hot[0]->category->name}}</div>
                    <a href="{{route('posts.show', [$hot[0]->category->slug, $hot[0]->slug])}}" style="z-index: 2;">
                        <div class="ind-news__banner-title" style="width: 100%;">{{$hot[0]->name}}</div>
                        <div class="ind-news__banner-text">{{$hot[0]->preview}}</div>
                    </a>
                    <div class="ind-news__banner-list">
                        <div class="swiper indexFairBannerSlider">
                            <div class="swiper-wrapper">
                                @foreach($hot as $i => $item)
                                @if($i)
                                <div class="swiper-slide">
                                    <a href="{{route('posts.show', [$item->category->slug, $item->slug])}}" class="ind-news__banner-list-item">
                                        <div class="ind-news__banner-item-line"></div>
                                        <div class="ind-news__banner-item-text">
                                            {{$item->name}}
                                        </div>
                                    </a>
                                </div>
                                @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </section>
    @if($cat1)
    <section class="ind-politic">
        <div class="ind-politic__title">
            <div class="containers">{{$cat1->name}}</div>
        </div>
        <div class="ind-politic__wrapper">
            <div class="containers">
                @foreach($pos1 as $key => $post)
                <div class="ind-politic__wrapper-block">
                    <div class="ind-politic__wrapper-block-img">
                    <img src="/storage/{{$post->image}}" alt="{{$post->name}}">
                    </div>
                    <div class="ind-politic__wrapper-block-text">
                        <a href="{{route('posts.show', [$post->category->slug, $post->slug])}}" class="ind-politic__wrapper-block-text-title">{{$post->name}}</a>
                        <div class="ind-politic__wrapper-block-text-line"></div>
                        <div class="ind-politic__wrapper-block-text-item">{{$post->preview}}</div>
                        <a href="{{route('posts.show', [$post->category->slug, $post->slug])}}" class="ind-politic__wrapper-block-text-btn">{{__('ui.readmore')}}</a>
                    </div>
                </div>
                @if(!$key) @break @endif
                @endforeach
                <div class="ind-politic__wrapper-list">
                    @foreach($pos1 as $key => $post)
                    @if($key > 0)
                    <a href="{{route('posts.show', [$post->category->slug, $post->slug])}}" class="ind-politic__wrapper-list-item">
                        <img src="/storage/{{$post->image}}" alt="{{$post->name}}">
                        <p>{{$post->preview}}</p>
                    </a>
                    @endif
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    @endif
    @if($cat2)
    <section class="ind-business">
        <div class="containers">
            <div class="ind-business__title">{{$cat2->name}}</div>
            <div class="ind-business__slider">
                <div class="swiper indexBusinessSlider">
                    <div class="swiper-wrapper">
                        @foreach($pos2 as $post)
                        <div class="swiper-slide">
                            <a href="{{route('posts.show', [$post->category->slug, $post->slug])}}" class="ind-business__slider-item">
                                <img src="/storage/{{$post->image}}" alt="{{$post->name}}">
                                <p>{{$post->name}}</p>
                            </a>
                        </div>
                        @endforeach
                    </div>
                    
                </div>
            </div>
            @if(0)
            <div class="ind-business__wrapper">
                <div class="ind-business__wrapper-item">
                    <img src="./img/index/business-4.png" alt="img">
                    <p>Schools, theaters, short skirts: 
                        what Afghanistan was like before the Taliban</p>
                </div>
                <div class="ind-business__wrapper-item">
                    <img src="./img/index/business-5.png" alt="img">
                    <p>Schools, theaters, short skirts: 
                        what Afghanistan was like before the Taliban</p>
                </div>
                <div class="ind-business__wrapper-item">
                    <img src="./img/index/business-6.png" alt="img">
                    <p>Schools, theaters, short skirts: 
                        what Afghanistan was like before the Taliban</p>
                </div>
            </div>
            @endif
        </div>
    </section>
    @endif
    <section class="ind-partner">
        <div class="containers">
            <div class="ind-partner__title">OUR PARTNERS</div>
            <div class="ind-partner__slider">
                <div class="swiper indexPartnerSlider">
                    <div class="swiper-wrapper">
                        @foreach($partners as $item)
                        <div class="swiper-slide"><img src="/storage/{{$item->image}}"></div>
                        @endforeach
                    </div>
                </div>
                <div class="swiper-controls">
                    <div class="swiper-button-next">
                        <div class="swiper-button-next-arrow"></div>
                    </div>
                    <div class="swiper-button-prev">
                        <div class="swiper-button-prev-arrow"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="ind-book">
        <div class="containers">
            <div class="ind-book__title">
                Buy books
            </div>
        </div>
        <div class="ind-book__slider">
            <div class="containers">
                <div class="swiper indexBookSlider">
                    <div class="swiper-wrapper">
                        @foreach($books as $book)
                        <div class="swiper-slide">
                            <div class="ind-book__slider-item">
                                <a href="{{route('books.show', $book->id)}}" class="ind-book__slider-item-img">
                                    <img src="/storage/{{$book->image}}" alt="{{$book->name}}">
                                </a>
                                <div class="ind-book__slider-item-add">{{$book->author}}</div>
                                <div class="ind-book__slider-item-title">{{$book->name}}</div>
                                <div class="ind-book__slider-item-line"></div>
                                <div class="ind-book__slider-item-price">{{$book->price}}$</div>
                                <div class="ind-book__slider-item-line"></div>
                                <button class="ind-book__slider-item-btns addtocart" data-id="{{$book->id}}">{{__('ui.buy')}}</button>
                                <button class="ind-book__slider-item-btn">{{__('ui.readdemo')}}</button>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="swiper-controls">
                    <div class="swiper-button-next">
                        <div class="swiper-button-next-arrow"></div>
                    </div>
                    <div class="swiper-button-prev">
                        <div class="swiper-button-prev-arrow"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop